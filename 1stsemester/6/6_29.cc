#include <iostream>
#include <string>
using namespace std;
struct Book{string title;int code;float price;};
struct Client{string name;int code;};
struct Sale{int client;int book;};

/*Dado un codigo de libro obtener cu ´ antos libros con ese c ´ odigo se han vendido. */
int sales(Sale m[],int n,int tsale){
	int x=0;
	for(int i=0;i<tsale;i++){
		if(m[i].book == n){x++;}
	return x;
	}
}
/*Dado un codigo de libro, obtener su posici ´ on en el vector de libros. */
int position(Book b[],int c,int tbooks){
	int x;
	for(int i=0;i<tbooks;i++){
		if(c==b[i].code){x=i;break;}
	return x;
	}

}
/*Obtener el t´ıtulo del libro mas vendido*/
string bestseller(Sale s[],Book b[],int tsale,int tbooks){
	struct Times{string title;int times;};
	Times books[1000];
	
	for(int i=0;i<tbooks;i++){
		books[i].title=b[i].title;
		books[i].times=0;
		for(int j=0;j<tsale;j++){
			if(b[i].code==s[j].book){
				books[i].times++;
			}
		}
	}
	int max=books[0].times;
	for(int i=1;i<tbooks;i++){
		if(books[i].times<max){max=books[i].times;}
	}
	for(int i=0;i<tbooks;i++){
		if(books[i].times==max){
			return books[i].title;
		}
	}
}



/*Obtener el t´ıtulo del libro que mas dinero ha recaudado.*/
string mostmoney(Book b[],Sale s[],int tbook,int tsale){
	struct Money{string title;int code;float money;};
	Money books[1000];
	for(int i=0;i<tbook;i++){
		books[i].title=b[i].title;
		books[i].code=b[i].code;
		for(int j=0;j<tbook;j++){
			if(books[i].code==s[j].book){books[i].money=books[i].money+b[i].price;}
		}
	}
	float max=0;
	string maximus;
	for(int i=0;i<tbook;i++){
		if(books[i].money>max){max=books[i].money;maximus=books[i].title;}
	}
	return maximus;
}

/*Dado un codigo de cliente obtener un vector con los t ´ ´ıtulos de los libros que ha
 * comprado.*/

int bookspurchased(Client c[],Sale s[],Book b[],int tbook,int tclient,int tsale,string* books){
	int bookcode[50];
	int num=0;
	for(int i=0;i<tclient;i++){
		for(int j=0;j<tsale;j++){
			if(c[i].code==s[j].client){
				bookcode[num]=s[j].book;
				num++;
			}
		}
	}
	int numbah=0;
	for(int i=0;i<num;i++){
		for(int j=0;i<tbook;j++){
			if(bookcode[i] == b[j].code){
				books[i]=b[j].title;
			}
		}
	}
	return num;
}



/*Obtener el nombre del cliente que ha comprado mas libros.*/

string b00kw0rm(Client c[],Sale s[],int tsale,int tclient){
	string x;
	struct Leclient{string name;int code;int purch;};
	Leclient clients[1000];
	for(int i=0;i<tclient;i++){
		clients[i].name = c[i].name;
		clients[i].code = c[i].code;
	}
	for(int i=0;i<tclient;i++){
		for(int j=0;j<tsale;j++){
			if(clients[i].code == s[j].client){
				clients[i].purch++;
			}
		}
	}
	x = clients[0].name;
	int max=clients[0].purch;
	for(int i=1;i<tclient;i++){
		if(clients[i].purch > max){
			x = clients[i].name;
		}
	}
	return x;
}

/*Obtener el nombre del cliente que ha gastado m ́as dinero.*/

string mostmoneyspentonbooks(Book b[],Client c[],Sale s[],int tsale,int tbooks,int tclient){
	string x;
	struct Leclient{string name;int code;int moneh;};
	Leclient clients[1000];
	for (int i=0;i<tclient;i++){
		clients[i].moneh=0;
	}
	for(int i=0;i<tclient;i++){
		clients[i].name = c[i].name;
		clients[i].code = c[i].code;
	}
	for(int i=0;i<tclient;i++){
		for(int j=0;j<tsale;j++){
			if(clients[i].code == s[j].client){
				for(int k=0;k<tbooks;k++){
					if(s[j].book == b[k].code){
						clients[i].moneh = clients[i].moneh + b[k].price;
					}
				}
			}
		}
	}
	x = clients[0].name;
	int max=clients[0].moneh;
	for(int i=1;i<tclient;i++){
		if(clients[i].moneh > max){
			x = clients[i].name;
		}
	}
	return x;
}

/*Realice un programa principal que permita probar las ultimas
cinco funciones.*/

/*
struct Book(string title,int code,float price);
struct Client(string name, int code);
struct Sale(int client, int book);
*/

int main(){
	Book books[1000]={{"Alice in Wonderland",323,30},{"Dante's inferno",666,60},{"Computers for dummies",101,45}};
	Client clients[1000]={{"Juanito",1},{"Pepito",2},{"Angustias",3},{"Pedro",4},{"Jose Botella",5},{"El rey de Espana",6}};
	Sale sales[1000]={{2,323},{1,323},{2,666},{3,101},{3,323},{5,666},{6,666},{6,323},{6,101},{4,101}};
	int totals=10,totalc=6,totalb=3;
/*
Obtener el t ́ıtulo del libro m ́as vendido.*/	

	cout << bestseller(sales,books,totals,totalb);
	cout << mostmoney(books,sales,totalb,totals);
	string bookz[1000];
	int newlength=bookspurchased(clients,sales,books,totalb,totalc,totals,bookz);
	cout << b00kw0rm(clients,sales, totals, totalc);
	cout << mostmoneyspentonbooks(books,clients,sales,totals,totalb,totalc);





	return 0;
}
