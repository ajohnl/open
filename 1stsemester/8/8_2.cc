#include <iostream>
using namespace std;
int main(){
	cout << "n=?\n";
	int n;
	cin >> n;
	int* fib=new int[n];
	fib[0]=0;
	fib[1]=1;
	for(int i=2;i<n;i++){
		//fib[i]=(*(fib+i-1))+*((fib+i-2));
		fib[i]=(fib[i-1]+fib[i-2]);
	}
	for(int i=0;i<n;i++){
		cout << fib[i] << "\n";
	}
	return 0;
}
